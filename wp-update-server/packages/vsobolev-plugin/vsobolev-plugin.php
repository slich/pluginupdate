<?php
/**
 * @package VSobolevPlugin
 * @
 * @
 */
 
/*
Plugin Name: VSobolev Plugin
Plugin URI: https://bitbucket.org/slich/pluginupdate/src/master/vsobolev-plugin.zip
Description: This is my first attempt of writing plugin update
Version: 1.2.4
Author: Sobolev Volodymyr
Author URI: https://bitbucket.org/slich/
License: GPLv2 or later
Text Domain: vsobolev-plugin
 */


//require 'plugin-update-checker/plugin-update-checker.php';
/*$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/slich/pluginupdate',
	__FILE__,
	'vsobolev-plugin'
);*/

//Optional: If you're using a private repository, create an OAuth consumer
//and set the authentication credentials like this:
//Note: For now you need to check "This is a private consumer" when
//creating the consumer to work around #134:
// https://github.com/YahnisElsts/plugin-update-checker/issues/134
/*$myUpdateChecker->setAuthentication(array(
	'consumer_key' => '...',
	'consumer_secret' => '...',
));*/

//Optional: Set the branch that contains the stable release.
//$myUpdateChecker->setBranch('master');

require 'plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'http://localhost/test/wp-update-server-master/?action=get_metadata&slug=vsobolev-plugin', //Metadata URL.
	__FILE__, //Full path to the main plugin file.
	'vsobolev-plugin' //Plugin slug. Usually it's the same as the name of the directory.
);